package com.hotdeploy;


import java.net.URI;
import java.net.URISyntaxException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.jboss.resteasy.core.Dispatcher;
import org.jboss.resteasy.mock.MockDispatcherFactory;
import org.jboss.resteasy.mock.MockHttpRequest;
import org.jboss.resteasy.mock.MockHttpResponse;
import org.jboss.resteasy.plugins.server.resourcefactory.POJOResourceFactory;
import org.junit.Assert;
import org.junit.Test;

import com.google.gson.JsonObject;

public class ClientTest {
	
	private ResteasyWebTarget target;
	private Dispatcher dispatcher;

	public ClientTest() {

		this.dispatcher = MockDispatcherFactory.createDispatcher();
		POJOResourceFactory noDefaults = new POJOResourceFactory(MyWebService.class);
		dispatcher.getRegistry().addResourceFactory(noDefaults);
	}

	// @Test
	public void connectionTest() throws URISyntaxException {

		String message = "mymessage";
		MockHttpRequest request = MockHttpRequest.get("/myservice/get-message/" + message);
		MockHttpResponse response = new MockHttpResponse();

		dispatcher.invoke(request, response);
		JsonObject jo = new JsonObject();
		jo.addProperty("message", message);
		Assert.assertEquals(jo.toString(), response.getContentAsString());
	}
	
	/**
	 * Example to test external web service
	 */
	// @Test
	public void testExternalService() {
		ResteasyClient client = new ResteasyClientBuilder().build();
		URI uri;
		try {
			uri = new URI("http://localhost:8080/hotdeploy/myservice");
			this.target = client.target(uri);
		} catch (URISyntaxException e) {
		}

		String message = "mymessage";
		Response response = this.target.path("/get-message/" + message).request().accept(MediaType.APPLICATION_JSON)
				.get();
		String value = response.readEntity(String.class);
		Assert.assertEquals("{\"message\":\"" + message + "\"}", value);
		response.close();
	}

	@Test
	public void postMessageTest() throws URISyntaxException {

		JsonObject jo = new JsonObject();
		jo.addProperty("name", "Tiago");
		jo.addProperty("email", "tiagowanke@gmail.com");

		MockHttpRequest request = MockHttpRequest.post("/myservice/post-message");
		request.accept(MediaType.APPLICATION_JSON);
		request.contentType(MediaType.APPLICATION_JSON);
		request.content(jo.toString().getBytes());

		MockHttpResponse response = new MockHttpResponse();
		dispatcher.invoke(request, response);
		Assert.assertEquals("Invalid response status", 200, response.getStatus());
	}

}
