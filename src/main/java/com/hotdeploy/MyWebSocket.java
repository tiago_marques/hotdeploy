package com.hotdeploy;

import java.io.IOException;
import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

import javax.inject.Inject;
import javax.websocket.CloseReason;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.hotdeploy.observer.Message;
import com.hotdeploy.observer.Messages;

@ServerEndpoint("/mywebsocket/{type}")
public class MyWebSocket implements Observer, Serializable {

	private static final long serialVersionUID = 6206353881371616560L;

	@Inject
	private MyService myService;
	@Inject
	private Messages messages;
	private Session session;

	@OnMessage
	public String hello(String message) {
		System.out.println("Received: " + message);
		return message;
	}

	@OnOpen
	public void myOnOpen(Session session, EndpointConfig config) {
		this.session = session;
		String type = session.getPathParameters().get("type");
		this.messages.addObserver(type, this);

		// Runnable runnable = new Runnable() {
		// @Override
		// public void run() {
				// try {
				// Object object = myService.myTestService().get(0);
				// Thread.sleep(1000);
				// session.getBasicRemote().sendText("Databese info. Id: " +
				// ((Object[]) object)[0] + ", name: " + ((Object[]) object)[1]
				// + ", email: " + ((Object[]) object)[2]);
				// } catch (IOException | InterruptedException e) {
				// e.printStackTrace();
				// }
		// }
		// };

		// Thread t = new Thread(runnable);
		// t.start();
		System.out.println("WebSocket open: " + session.getId());
	}

	@OnClose
	public void myOnClose(CloseReason reason) {
		System.out.println("Closing a WebSocket due to " + reason.getReasonPhrase());
		this.messages.deleteObserver(this);
	}

	@Override
	public void update(Observable o, Object arg) {

		try {
			this.session.getBasicRemote().sendText(((Message) o).getMessage());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
