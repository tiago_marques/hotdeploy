package com.hotdeploy;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.shiro.SecurityUtils;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.hotdeploy.model.MyEntity;
import com.hotdeploy.observer.Messages;

@Path("/myservice")
public class MyWebService {

	@Inject
	private MyService myService;

	@Inject
	private Messages messages;

	@GET
	@Path("/get-message/{param}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMessage(@PathParam("param") String msg) {
		List<Object> myTestService = myService.myTestService();
		JsonObject jo = new JsonObject();
		jo.addProperty("id", (Number) ((Object[]) myTestService.get(0))[0]);
		jo.addProperty("name", ((Object[]) myTestService.get(0))[1].toString());
		jo.addProperty("email", ((Object[]) myTestService.get(0))[2].toString());
		return Response.ok(jo.toString()).build();
	}

	@POST
	@Path("/post-message")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response postMessage(MyEntity myEntity) {
		return Response.ok().build();
	}

	@POST
	@Path("/send-message")
	@Consumes(MediaType.TEXT_PLAIN)
	public Response sendMessage(String message) {
		JsonObject jo = new JsonParser().parse(message).getAsJsonObject();
		this.messages.sendMessage(jo.get("type").getAsString(), jo.get("message").getAsString());
		return Response.ok().build();
	}

	@GET
	@Path("/logout")
	public Response logout() {
		SecurityUtils.getSubject().logout();
		return Response.ok().build();
	}
}