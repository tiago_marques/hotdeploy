package com.hotdeploy;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

public class MyService implements Serializable {

	private static final long serialVersionUID = -6650007464248712805L;
	@PersistenceContext
	EntityManager em;

	public List<Object> myTestService() {
		System.out.println("this is my test service ");
		Query query = em.createNativeQuery("SELECT * FROM user");
		return query.getResultList();
	}
}
