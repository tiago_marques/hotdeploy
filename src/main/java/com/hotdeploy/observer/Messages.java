package com.hotdeploy.observer;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Observer;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class Messages implements Serializable {

	private static final long serialVersionUID = 2377322143104114252L;

	private Map<String, Message> messages = new HashMap<String, Message>();

	private Message getMessageByType(String type) {
		Message message = this.messages.get(type);
		if (message == null) {
			message = new Message();
			this.messages.put(type, message);
		}
		return message;
	}

	public void sendMessage(String type, String msg) {
		getMessageByType(type).sendMessage(msg);
	}

	public void addObserver(String type, Observer ob) {
		getMessageByType(type).addObserver(ob);
	}

	public void deleteObserver(Observer ob) {
		Message message = getMessageByType("ADMIN");
		message.deleteObserver(ob);
		if (message.countObservers() == 0) {
			this.messages.remove(message);
		}

		message = getMessageByType("USER");
		message.deleteObserver(ob);
		if (message.countObservers() == 0) {
			this.messages.remove(message);
		}
	}

}
