package com.hotdeploy.observer;

import java.io.Serializable;
import java.util.Observable;

public class Message extends Observable implements Serializable {

	private static final long serialVersionUID = -1496708022851050048L;
	private String message;

	public void sendMessage(String message) {
		this.message = message;
		setChanged();
		notifyObservers();
	}

	public void clearMessage() {
		this.message = null;
	}

	Message() {
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
