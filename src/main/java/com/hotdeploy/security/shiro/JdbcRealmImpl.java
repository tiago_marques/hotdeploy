package com.hotdeploy.security.shiro;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.realm.jdbc.JdbcRealm;

public class JdbcRealmImpl extends JdbcRealm {

	public JdbcRealmImpl() {
		super();
	}

	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(final AuthenticationToken token)
			throws AuthenticationException {
		final AuthenticationInfo info = super.doGetAuthenticationInfo(token);
		return new SimpleAuthenticationInfo(token.getPrincipal(), info.getCredentials(), getName());
	}
}
